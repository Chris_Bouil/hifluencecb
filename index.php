<?php
session_start();
include_once 'dbconnect.php';

if( (isset($_SESSION['userSession'])!="") && (isset($_SESSION['FBID'])!="") )
{
	header("Location: home.php");
	exit;
}

if(isset($_POST['btn-login']))
{
	$uname = $MySQLi_CON->real_escape_string(trim($_POST['user_name']));
	$upass = $MySQLi_CON->real_escape_string(trim($_POST['password']));
	
	$query = $MySQLi_CON->query("SELECT user_id, user_name, user_pass FROM users WHERE user_name='$uname'");
	$row=$query->fetch_array();
	
	if(password_verify($upass, $row['user_pass']))
	{
		$_SESSION['userSession'] = $row['user_id'];
		header("Location: home.php");
	}
	else
	{
		$msg = "<div class='alert alert-danger'>
					<span class='glyphicon glyphicon-info-sign'></span> &nbsp; email or password does not exists !
				</div>";
	}
	
	$MySQLi_CON->close();
	
}



if(isset($_POST['btn-signup']))
{
	$target_dir = "uploads/";
	$target_file = $target_dir . basename($_FILES["file"]["name"]);
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

	$check = getimagesize($_FILES["file"]["tmp_name"]);
    if($check !== false) {
        $uploadOk = 1;
    } else {
        $uploadOk = 0;
    }



	$ufile = $target_file;
	$uname = $MySQLi_CON->real_escape_string(trim($_POST['user_name']));
	$email = $MySQLi_CON->real_escape_string(trim($_POST['user_email']));
	$upass = $MySQLi_CON->real_escape_string(trim($_POST['password']));
	
	$new_password = password_hash($upass, PASSWORD_DEFAULT);
	
	$check_email = $MySQLi_CON->query("SELECT user_email FROM users WHERE user_email='$email'");
	$count=$check_email->num_rows;
	
	if($count==0 && $uploadOk==1){
		
		
		$query = "INSERT INTO users(user_name,user_email,user_pass,user_file) VALUES('$uname','$email','$new_password','$ufile')";

		
		if($MySQLi_CON->query($query))
		{
			$msg1 = "<div class='alert alert-success'>
						<span class='glyphicon glyphicon-info-sign'></span> &nbsp; successfully registered ! You can now log in.
					</div>";
		}
		else
		{
			$msg1 = "<div class='alert alert-danger'>
						<span class='glyphicon glyphicon-info-sign'></span> &nbsp; error while registering !
					</div>";
		}
	}

	else{
		
		// Check file size
		if ($_FILES["file"]["size"] > 500000) {
		    $msg1 = "<div class='alert alert-danger'>
						<span class='glyphicon glyphicon-info-sign'></span> &nbsp; your file is too large.
					</div>";
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
		    $msg1 = "<div class='alert alert-danger'>
						<span class='glyphicon glyphicon-info-sign'></span> &nbsp; only JPG, JPEG, PNG & GIF files are allowed.
					</div>";
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    $msg1 .= "<div class='alert alert-danger'>
						<span class='glyphicon glyphicon-info-sign'></span> &nbsp; sorry, your file was not uploaded.
					</div>";
		// if everything is ok, try to upload file
		} 
		else {
			$msg1 = "<div class='alert alert-danger'>
						<span class='glyphicon glyphicon-info-sign'></span> &nbsp; sorry email already taken !
					</div>";
		}
	}
	
	$MySQLi_CON->close();
}

?>
<!DOCTYPE html >
<html xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Opdracht Hifluence - CB</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="css/style.css" type="text/css" />
<script src="js/jstorage.js" type="text/javascript" charset="utf-8"></script>
<script src="js/script.js" type="text/javascript" charset="utf-8"></script>
<link href="css/jquery.ezdz.css" rel="stylesheet" type="text/css">
<script src="js/jquery.ezdz.js"></script>

</head>
<body>

	<div class="container signin">
		<div id="logo">
			<img src="img/icon.png"/>
		</div>
		<div class="signin-form">
			
			<header class="row">
				<h1>Login to your account / Register new</h1>
			</header>
			<div class="content row">
		     	<div class="col-sm-6 wrapper-login">
		  			<div class="fb-login">
						<a href="fbconfig.php" class="fb-btn"><img src="img/facebookconnect.png" alt="Login with Facebook"/></a>
		  			</div>

					<div class="seperator">
						<span class="line line-left">&nbsp;</span>
						<span class="icon">&#47</span>
						<span class="line line-right">&nbsp;</span>
					</div>

					<form class="form-signin" method="post" id="login-form">
						
						<div class="form-group">
							<?php
								if(isset($msg)){
								echo $msg;
							}
							?>
						</div>

						<div class="form-group form-field">
							<label for="user_name">Username</label>
							<input type="text" class="form-control" id="username" placeholder="Username" name="user_name" required />
						</div>

						<div class="form-group form-field">
							<label for="password">Password</label>
							<input type="password" class="form-control" id="password" placeholder="Password" name="password" required />
						</div>

						<div class="form-group form-remember form-action">
							<label><input type="checkbox" id="remember" name="remember">Remember my password</label>
							<button type="submit" class="btn btn-default" name="btn-login" id="btn-login">
								Login
							</button> 
						</div>  
					</form>
				</div>
		      	<div class="col-sm-6 wrapper-register">
		      		<h2>Register</h2>
		      		<form class="form-signin " method="post" id="register-form" enctype="multipart/form-data">
				        
				        <div class="form-group dropzone">
							<label for="file"> Profile image</label>
							<input type="file" name="file" id="file"/>

				        </div>
				        <div class="form-group form-field">
				        	<label for="user_name">Username</label>
					        <input type="text" class="form-control" placeholder="Username" name="user_name" required  />
				        </div>

				        <div class="form-group form-field">
				        	<label for="user_email">Email</label>
					        <input type="email" class="form-control" placeholder="Email address" name="user_email" required  />
				        </div>
				        
				        <div class="form-group form-field">
				        	<label for="password">Password</label>
				        	<input type="password" class="form-control" placeholder="Password" name="password" required  />
				        </div>
				        
				        <div class="form-group">
					     	<?php
							if(isset($msg1)){
								echo $msg1;
							}
							?>
						</div>
				        
				        <div class="form-group form-action">
				            <button type="submit" class="btn btn-default" name="btn-signup" id="btn-signup">
				    			Register
							</button>
				            
				        </div> 
				      
				    </form>
		      	</div>
		    </div>
	    </div>
	</div>

<script>
	$('#file').ezdz({
        text: 'Drop here',
        className: 'drop-zone',

    });
</script>
</body>
</html>