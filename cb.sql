-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Machine: localhost
-- Gegenereerd op: 28 feb 2016 om 21:55
-- Serverversie: 5.6.20
-- PHP-versie: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `cb`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
`comment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment_name` varchar(255) NOT NULL,
  `comment_email` varchar(255) NOT NULL,
  `comment_text` varchar(500) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Gegevens worden geëxporteerd voor tabel `comments`
--

INSERT INTO `comments` (`comment_id`, `user_id`, `comment_name`, `comment_email`, `comment_text`) VALUES
(1, 12, 'test', 'test@mail.com', 'testing now'),
(10, 12, 'Chris', 'test@mail.com', 'testing'),
(16, 16, 'Christophe', 'chris@mail.com', 'testen');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(11) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(55) DEFAULT NULL,
  `user_pass` varchar(255) DEFAULT NULL,
  `fbid` varchar(100) NOT NULL,
  `fbfullname` varchar(60) NOT NULL,
  `femail` varchar(60) DEFAULT NULL,
  `user_file` varchar(200) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_email`, `user_pass`, `fbid`, `fbfullname`, `femail`, `user_file`) VALUES
(3, 'Chris_Bouil', 'chris.bouillon@gmail.com', '$2y$10$aaPtVVeg3m49uWR/JL6riOckTxINhGWFhGzSDJleBDSzgoAa.nEAa', '', '', NULL, ''),
(12, 'test1', 'test1@mail.com', '$2y$10$LYl7xj8yUrhbzkBs0P7cDu5hEPmUssTks6dl8rkExrK79U70cSwX2', '', '', NULL, 'uploads/12249678_10156306213630253_7755308246257794966_n (1).jpg'),
(13, 'test2', 'test2@mail.com', '$2y$10$Di1fdIFVKWF3NG7Uq.XQsOjBRIaSa2q.p34Yb.mFOztWY.ycWB4Pq', '', '', NULL, 'uploads/5e6156a0-1990-11e4-8130-7595186f6781.jpg'),
(16, 'Christophe Bouillon', '', NULL, '10156625441990253', '', NULL, 'https://graph.facebook.com/10156625441990253/picture');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `comments`
--
ALTER TABLE `comments`
 ADD PRIMARY KEY (`comment_id`), ADD KEY `user_id` (`user_id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_id`), ADD UNIQUE KEY `email` (`user_email`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `comments`
--
ALTER TABLE `comments`
MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
