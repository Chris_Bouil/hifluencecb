<?php
session_start();

if( (!isset($_SESSION['userSession'])) || (!isset($_SESSION['FBID'])) )
{
	header("Location: index.php");
}
else if( (isset($_SESSION['userSession'])!="") || (isset($_SESSION['FBID'])!="") )
{
	header("Location: home.php");
}

if(isset($_GET['logout']))
{
	session_destroy();
	unset($_SESSION['userSession']);
	$_SESSION['FBID'] = NULL;
    $_SESSION['FULLNAME'] = NULL;
    $_SESSION['EMAIL'] =  NULL;
	header("Location: index.php");
}
if (isset($_GET['justloggedout'])) {
    $facebook->destroySession();
    session_unset();
    session_destroy();
    header("Location: index.php");
}
?>
