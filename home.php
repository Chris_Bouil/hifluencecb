<?php
session_start();
include_once 'dbconnect.php';

if( (!isset($_SESSION['userSession'])) && (!isset($_SESSION['FBID'])) )
{
	header("Location: index.php");
}

if(isset($_SESSION['userSession'])) {
	$query = $MySQLi_CON->query("SELECT * FROM users WHERE user_id=".$_SESSION['userSession']);
	$userRow=$query->fetch_array();

	$select = $MySQLi_CON->query("SELECT * FROM comments WHERE user_id=".$_SESSION['userSession']);
	$results = "";

	while($row=$select->fetch_array())
	{
	 $results .= "
	 	<li>
	 		<div class='comment-name'><strong>".$row['comment_name']." (".$row['comment_email'].")</strong></div>
	 		<p class='comment-text'>".$row['comment_text']."</p>
	 	</li>
	 ";
	}
}

if(isset($_SESSION['FBID'])) {
	$query = $MySQLi_CON->query("SELECT * FROM users WHERE fbid=".$_SESSION['FBID']);
	$userRow=$query->fetch_array();

	$select = $MySQLi_CON->query("SELECT * FROM comments WHERE user_id=".$userRow['user_id']);
	$results = "";

	while($row=$select->fetch_array())
	{
	 $results .= "
	 	<li>
	 		<div class='comment-name'><strong>".$row['comment_name']." (".$row['comment_email'].")</strong></div>
	 		<p class='comment-text'>".$row['comment_text']."</p>
	 	</li>
	 ";
	}

}

if(isset($_POST['btn-comment'])) {
	$c_name = $MySQLi_CON->real_escape_string(trim($_POST['comment_name']));
	$c_email = $MySQLi_CON->real_escape_string(trim($_POST['comment_email']));
	$c_text = $MySQLi_CON->real_escape_string(trim($_POST['comment_text']));
	
	if(isset($_SESSION['FBID'])) {
		$query = "INSERT INTO comments(user_id,comment_name,comment_email,comment_text) VALUES (".$userRow['user_id'].",'$c_name','$c_email','$c_text')";
	}
	if(isset($_SESSION['userSession'])) {
		$query = "INSERT INTO comments(user_id,comment_name,comment_email,comment_text) VALUES (".$_SESSION['userSession'].",'$c_name','$c_email','$c_text')";
	}
		
	if($MySQLi_CON->query($query))
	{
		$msg = "<div class='alert alert-success'>
					<span class='glyphicon glyphicon-info-sign'></span> &nbsp; Succes! Your comment has been received.
				</div>";
	}
	else
	{
		$msg = "<div class='alert alert-danger'>
					<span class='glyphicon glyphicon-info-sign'></span> &nbsp; Error, with your comment !
				</div>";
	}
	$MySQLi_CON->close();
}
?>
<!DOCTYPE html >
<html xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Welcome 
<?php  
if (isset($userRow['user_name'])) {
	echo $userRow['user_name']; 
}
else {
	echo $_SESSION['FULLNAME'];
}
?> - Opdracht Hifluence CB</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="css/style.css" type="text/css" />
<script src="js/jstorage.js" type="text/javascript" charset="utf-8"></script>
<script src="js/script.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>
  <div class="container">
  
    
	<div class="jumbotron">
		<h1>Hello <?php echo $userRow['user_name']; ?></h1>
	</div>
	<div class="user-image col-sm-3">
		<img src="<?php echo $userRow['user_file']; ?>">
	</div>
	<ul class="user-list col-sm-9 list-unstyled">
		<li class="nav-header">Username: <?php echo $userRow['user_name']; ?></li>
		<li class="nav-header">Email: <?php echo $userRow['user_email']; ?></li>
		<li><a href="logout.php?logout" class="btn btn-primary">Logout</a></li>
	</ul>
    
    <div class="comment-container col-sm-9">
    	<h3>Comment</h3>
    	<?php if(isset($results)): ?>
    	<div class="comment-list">
    		<ul class="list-unstyled col-sm-12">
    			<?php echo $results; ?>
    		</ul>
    	</div>
    	<?php endif ?>
    	<form class="form-comment " method="post" id="comment-form">
			        
	        <div class="form-group form-field col-sm-6">
	        	<label for="comment_name">Username</label>
		        <input type="text" class="form-control" placeholder="Name" name="comment_name" required  />
	        </div>

	        <div class="form-group form-field col-sm-6">
	        	<label for="comment_email">Email</label>
		        <input type="email" class="form-control" placeholder="Email address" name="comment_email" required  />
	        </div>
	        
	        <div class="form-group form-field col-sm-12">
	        	<label for="comment_text">Comment Text</label>
	        	<textarea type="password" class="form-control" placeholder="Comment.." name="comment_text" required ></textarea>
	        </div>
	        
	        <div class="form-group col-sm-12">
		     	<?php
				if(isset($msg)){
					echo $msg;
				}
				?>
			</div>
	        
	        <div class="form-group">
	            <button type="submit" class="btn btn-primary" name="btn-comment" id="btn-comment">
	    			Submit
				</button>
	            
	        </div> 
	      
	    </form>
    </div>

  </div>

</body>
</html>